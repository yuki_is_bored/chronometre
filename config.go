package main

import (
	"os"
	"path"

	"gopkg.in/ini.v1"
)

// Config is a structure of the configuration file.
type Config struct {
	*Meta     `comment:"Do not change *ANY* values in this section!"`
	*Server   `comment:"HTTP Server Configuration"`
	*Database `comment:"SQL Database documentation"`
}

// Meta represents the meta-data section of the configuration file.
// It holds variables which are not meant to be modified by the user.
type Meta struct {
	// Version indicates the iteration of the configuration file structure.
	Version int
}

type Server struct {
	// Address represents the listening address the Gin engine will be using.
	Address string `comment:"The IP address and port where the daemon is listening to. Default: '0.0.0.0:8080'."`

	// Debug indicates if Gin's debug mode is enabled or not.
	Debug   bool   `comment:"Enable more verbose logging. Useful for developers. Default: 'false'."`
}

type Database struct {
	// Driver indicates which SQL driver will be used
	Driver string `comment:"The database driver being used. Available: 'postgres', 'mysql', 'sqlite3'. Default: 'sqlite3'."`
	// Source represents the address of the data source. This varies by drivers.
	Source string `comment:"The source of the database. Default: './chronometre.db'."`
}

// cfg is an instance of Config
var cfg *Config

// initConfig loads the configuration file to the 'conf' variable.
// If it doesn't exist, a default configuration file will be created.
func initConfig(fpath string) error {
	cfg = new(Config)

	if err := ini.MapTo(cfg, fpath); err != nil {
		if os.IsNotExist(err) {
			return createDefaultConfig(fpath)
		}
		return err
	}

	return nil
}

// createDefaultConfig creates a default configuration file.
func createDefaultConfig(fpath string) error {
	cfg = &Config{
		Meta: &Meta{
			Version: 0,
		},
		Server: &Server{
			Address: "0.0.0.0:8080",
			Debug: false,
		},
		Database: &Database{
			Driver: "sqlite3",
			Source: "./database.sqlite3",
		},
	}

	if err := os.MkdirAll(path.Dir(fpath), 0755); err != nil {
		return err
	}

	fini := ini.Empty()

	if err := fini.ReflectFrom(cfg); err != nil {
		return err
	}

	err := fini.SaveTo(fpath)

	return err;
}
