package main

import (
	"flag"
	"log"
)

var (
	cfgPath string
)

// init declares the command line flags
func init() {
	flag.StringVar(&cfgPath,
		"config", "./config.ini", "Path to configuration file")
}

// main contains the initialization instance and finally starts the server
func main() {
	flag.Parse()

	if err := initConfig(cfgPath); err != nil {
		log.Panicf("error initialising config: %v", err)
	}

	startServer()
}
