# Chronomètre

Free and Open Source Self-Hosted Time Tracking Software

## Planned Features

Planned features are categorized per milestones

### Foundation

- Time tracking entries on your own server
- Complete REST API
- Support for SQLite, MySQL, and PostgreSQL

### Essential

- JSON/CSV Report Generation
- Live Timer
- Basic multi-user support

### Collaborate

- Mature multi-user support
- Teams / Workspaces

### Network

- Data Synchronization between instances for distributed teams
- OAuth2/LDAP Authentication

## License

Licensed under MIT.
