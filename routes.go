package main

import (
	"github.com/gin-gonic/gin"
)

// startServer setups the routes and starts the Gin engine
func startServer() {
	if !cfg.Server.Debug {
		gin.SetMode(gin.ReleaseMode)
	}

	r := gin.Default()

	routes(r)

	r.Run(cfg.Server.Address)
}

// routes declares the routes to the Gin Engine
func routes(r *gin.Engine) {
	r.GET("/", indexEndpoint)
}
