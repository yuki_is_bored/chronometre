# The Chronomètre Project

Chronomètre is a free and open source self-hosted time tracking software.

Our target audience are distributed teams which require a time tracker the most
in order to work but we also target individuals because time tracking is a good
habit to have and helps with time management.

Existing solutions are mostly non-free online services. We want a time tracking
solution which is free, open source, and self-hosted.

We consider decentralized/distributed systems to be the way of the future. More
than just being free from ridiculous monthly fees, it gives us control and free
from anti-features that locks us into their platforms.

## Design choice

### Web Backend API

We choose to be a backend application because we will be able to use it with
other software such as a web frontend, a mobile/desktop application, etc. It
enables more integration with other software such as a ticketing system, an
invoice system, etc.

#### REST + JSON

We choose REST + JSON due to their high usage and interoperability. The majority
of services are using REST + JSON as the means of communication on their API.
Thanks to the availability of various libraries for various languages which
enables the usage of REST + JSON, it has become a "standard" which we're happy
to comply.

### Go

We choose the Go programming language due to it's high performance and ease of
use. The Go programming language promotes simplicity without keeping things
oversimplified. It ressembles C but a much friendlier version of it. The
documentation are short and on point. It promotes good practices by forcing them
in the compiler and documentation with it's built-in documentation system.

### Gin

We choose the Gin web framework because it is a micro-framework meaning it
contains the basics we need along with other useful stuff. We're in favour
of micro-frameworks instead of traditional MVC frameworks because they contain
too many stuff that we won't need and can result in extra head aches due to the
massive code base of the library.

### Ini

We choose ini files because they have been used for decades as configuration
files. The simplicity of the file format is one of it's biggest advantage.
The library we'll be using is go-ini. go-ini is made for configuration files.
Thanks to the API, it enables more flexibility in the future and helps with
migrating each iteration of configuration files. It also has comforting features
such as being able to work on comments which enables self-documentating default
configuration.

## Definitions

### Workspace

A Workspace contains a compilation of time entries. For now, each user has their
own workspaces but in the future, it will be possible to have multiple users on
a single workspace allowing entries for a single team. The workspace also
contains a list of projects and tags.

### Time Entry

A Time entry is composed of a start and end "time" stored as UNIX timestamps
along with a project and a description and who did it. Tags are added as well
in order to have extra pre-defined attributes.

### Project

A project is composed of a name as a string and an optional "client" or
"customer" name.

### Tag

A tag is simply a string and an HTML color code. Tags are used simply to have
pre-defined attributes such as "Is this entry 'billable'?" or "What kind of work
is being done?".

----

This design document is still under construction due to the project being in an
early stage of development. Changes will be made.
