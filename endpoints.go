package main

import (
	"github.com/gin-gonic/gin"
)

// indexEndpoint is the endpoint for the HTTP index.
func indexEndpoint(c *gin.Context) {
	// TODO: Since this is an really early iteration of the project. We will need to change this in the future

	c.JSON(200, gin.H{
		"message": "It works!",
	})
}
